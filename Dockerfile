FROM archlinux:latest

RUN pacman -Syu base-devel git python python-pip python-setuptools gcc procps-ng xterm wget tk python-wxpython python-numpy --noconfirm && pacman -Sc --noconfirm

# Create a user
RUN useradd -m akl

# Give passwordless sudo access
RUN echo "akl ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# From this point, the container runs as the `akl` user is logged in
USER akl
WORKDIR /home/akl
RUN pip install wheel pymavlink mavproxy opencv-python empy pexpect --no-cache-dir

# From now on, following the official guide: https://github.com/ArduPilot/ardupilot/blob/master/BUILD.md
RUN git clone --recursive https://github.com/ArduPilot/ardupilot.git --depth 1
WORKDIR /home/akl/ardupilot

RUN ./waf configure
RUN ./waf copter
RUN ./waf plane

FROM archlinux:latest

RUN pacman -Syu gcc procps-ng git python python-pip python-setuptools xterm wget tk python-wxpython python-numpy --noconfirm && pacman -Sc --noconfirm

RUN useradd -m akl
RUN mkdir /home/akl/ardupilot
RUN pip install opencv-python wheel pymavlink mavproxy pexpect matplotlib --no-cache-dir

COPY --from=0 /home/akl/ardupilot /home/akl/ardupilot

USER akl
WORKDIR /home/akl/ardupilot/Tools/autotest

# Custom AKL locations
RUN echo "Legnica=51.18268,16.17713,113,80" >> locations.txt

# Disable pre-arm checks, I don't care
# https://ardupilot.org/copter/docs/common-prearm-safety-checks.html
RUN echo "ARMING_CHECK    0" >> default_params/plane.parm
RUN echo "ARMING_CHECK    0" >> default_params/copter.parm
